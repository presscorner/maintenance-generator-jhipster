# Outils

  * Auteur : Dionisie Prescornic
  * Date de publication : Mars 2020

## Dimension "Équipe de développement"

1. [Gitinspector](https://github.com/ejwa/gitinspector) - outil d'analyse statistique pour les dépôts Git.

  - Installation Ubuntu :
      ```console
	  $ sudo apt install gitinspector
	  ```
  - Qui sont les développeurs principaux du projet ? 
      ```console
	  $ gitinspector --format=html > commits_by_author_stats.html
	  $ google-chrome commits_by_author_stats.html
	  ```
	  
2. [Le plugin pour IntelliJ CodeMR](https://plugins.jetbrains.com/plugin/10811-codemr) - outil d'analyse statistique du code.

  - Installation dans IntelliJ 
